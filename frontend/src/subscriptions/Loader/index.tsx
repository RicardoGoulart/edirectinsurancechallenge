import { BehaviorSubject } from 'rxjs';

class LoaderSubscription {
  loader = new BehaviorSubject(true);

  show = () => {
    this.loader.next(true);
  };

  hide = () => {
    this.loader.next(false);
  };
};

export default new LoaderSubscription();