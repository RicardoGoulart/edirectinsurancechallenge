import * as React from "react";
import { LoaderSettings } from './../../interfaces';
import LoaderSubscription from './../../subscriptions/Loader';

import './index.scss';

export class CustomLoader extends React.Component {
  state:LoaderSettings = { 
      showLoader: true
  };

  // Holds the subscription
  loaderSubscription:any;
  loaderClasses:string = "loaderwrapper";
  disabledClass:string = "disabled";

  componentDidMount = () => {
    this.activateSubscriptions();
  };

  componentWillUnmount = () => {
    this.killSubscriptions();
  };

  activateSubscriptions = () => {
    this.loaderSubscription = LoaderSubscription.loader.subscribe(this.handleLoader);
  };

  killSubscriptions = () => {
    this.loaderSubscription.unsubscribe();
  };

  handleLoader = ( state:boolean ) => {
    this.setState({
      showLoader: state
    });
  };

  displayClass = () => {
    return this.state.showLoader ? this.loaderClasses : this.loaderClasses.concat( ' ', this.disabledClass );
  };

  render() {
    return (<div className={this.displayClass()}>Loader</div>);
  }
}