import React from 'react';
import LoaderSubscription from './subscriptions/Loader';
import {CustomLoader} from './components';


// Resourses
import './App.css';

class App extends React.Component {
  componentDidMount = () => {
    setTimeout( () => {
      LoaderSubscription.hide();
    }, 2500 );
  };

  render(){
    return (
      <div className="App">
        <header className="App-header">
          
        </header>
        <CustomLoader />
      </div>
    );
  }
}

export default App;
