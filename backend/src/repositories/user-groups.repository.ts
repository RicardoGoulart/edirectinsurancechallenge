import {DefaultCrudRepository} from '@loopback/repository';
import {UserGroups, UserGroupsRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserGroupsRepository extends DefaultCrudRepository<
  UserGroups,
  typeof UserGroups.prototype.id,
  UserGroupsRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(UserGroups, dataSource);
  }
}
