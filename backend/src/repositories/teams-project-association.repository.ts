import {DefaultCrudRepository} from '@loopback/repository';
import {
  TeamsProjectAssociation,
  TeamsProjectAssociationRelations,
} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TeamsProjectAssociationRepository extends DefaultCrudRepository<
  TeamsProjectAssociation,
  typeof TeamsProjectAssociation.prototype.id,
  TeamsProjectAssociationRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(TeamsProjectAssociation, dataSource);
  }
}
