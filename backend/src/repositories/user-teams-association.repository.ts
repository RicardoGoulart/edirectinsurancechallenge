import {DefaultCrudRepository} from '@loopback/repository';
import {UserTeamsAssociation, UserTeamsAssociationRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserTeamsAssociationRepository extends DefaultCrudRepository<
  UserTeamsAssociation,
  typeof UserTeamsAssociation.prototype.id,
  UserTeamsAssociationRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(UserTeamsAssociation, dataSource);
  }
}
