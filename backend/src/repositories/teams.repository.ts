import {DefaultCrudRepository} from '@loopback/repository';
import {Teams, TeamsRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class TeamsRepository extends DefaultCrudRepository<
  Teams,
  typeof Teams.prototype.id,
  TeamsRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(Teams, dataSource);
  }
}
