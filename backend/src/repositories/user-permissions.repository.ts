import {DefaultCrudRepository} from '@loopback/repository';
import {UserPermissions, UserPermissionsRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserPermissionsRepository extends DefaultCrudRepository<
  UserPermissions,
  typeof UserPermissions.prototype.id,
  UserPermissionsRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(UserPermissions, dataSource);
  }
}
