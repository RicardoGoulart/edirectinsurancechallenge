import {DefaultCrudRepository} from '@loopback/repository';
import {UserGroupPermissions, UserGroupPermissionsRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserGroupPermissionsRepository extends DefaultCrudRepository<
  UserGroupPermissions,
  typeof UserGroupPermissions.prototype.id,
  UserGroupPermissionsRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(UserGroupPermissions, dataSource);
  }
}
