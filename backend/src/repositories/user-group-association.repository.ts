import {DefaultCrudRepository} from '@loopback/repository';
import {UserGroupAssociation, UserGroupAssociationRelations} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class UserGroupAssociationRepository extends DefaultCrudRepository<
  UserGroupAssociation,
  typeof UserGroupAssociation.prototype.id,
  UserGroupAssociationRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(UserGroupAssociation, dataSource);
  }
}
