import {DefaultCrudRepository} from '@loopback/repository';
import {
  ProjectTasksAssociation,
  ProjectTasksAssociationRelations,
} from '../models';
import {MemoryDbDataSource} from '../datasources';
import {inject} from '@loopback/core';

export class ProjectTasksAssociationRepository extends DefaultCrudRepository<
  ProjectTasksAssociation,
  typeof ProjectTasksAssociation.prototype.id,
  ProjectTasksAssociationRelations
> {
  constructor(@inject('datasources.MemoryDB') dataSource: MemoryDbDataSource) {
    super(ProjectTasksAssociation, dataSource);
  }
}
