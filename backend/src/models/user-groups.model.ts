import {Entity, model, property} from '@loopback/repository';

@model()
export class UserGroups extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  constructor(data?: Partial<UserGroups>) {
    super(data);
  }
}

export interface UserGroupsRelations {
  // describe navigational properties here
}

export type UserGroupsWithRelations = UserGroups & UserGroupsRelations;
