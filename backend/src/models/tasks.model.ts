import {Entity, model, property} from '@loopback/repository';

@model()
export class Tasks extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
  })
  title?: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'date',
  })
  startdate?: string;

  @property({
    type: 'date',
  })
  enddate?: string;

  @property({
    type: 'number',
  })
  reporteruserid?: number;

  @property({
    type: 'number',
  })
  approveruserid?: number;

  @property({
    type: 'number',
  })
  responsibleuserid?: number;

  @property({
    type: 'number',
  })
  hoursestimate?: number;

  @property({
    type: 'number',
  })
  hoursspent?: number;

  @property({
    type: 'boolean',
    required: true,
    default: true,
  })
  active: boolean;

  @property({
    type: 'boolean',
    required: true,
    default: false,
  })
  completed: boolean;

  constructor(data?: Partial<Tasks>) {
    super(data);
  }
}

export interface TasksRelations {
  // describe navigational properties here
}

export type TasksWithRelations = Tasks & TasksRelations;
