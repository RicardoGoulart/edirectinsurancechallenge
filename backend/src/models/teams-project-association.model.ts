import {Entity, model, property} from '@loopback/repository';

@model()
export class TeamsProjectAssociation extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  teamid: number;

  @property({
    type: 'number',
    required: true,
  })
  projectid: number;

  constructor(data?: Partial<TeamsProjectAssociation>) {
    super(data);
  }
}

export interface TeamsProjectAssociationRelations {
  // describe navigational properties here
}

export type TeamsProjectAssociationWithRelations = TeamsProjectAssociation &
  TeamsProjectAssociationRelations;
