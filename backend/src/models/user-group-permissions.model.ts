import {Entity, model, property} from '@loopback/repository';

@model()
export class UserGroupPermissions extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  groupid: number;

  @property({
    type: 'number',
    required: true,
  })
  permissionid: number;

  constructor(data?: Partial<UserGroupPermissions>) {
    super(data);
  }
}

export interface UserGroupPermissionsRelations {
  // describe navigational properties here
}

export type UserGroupPermissionsWithRelations = UserGroupPermissions &
  UserGroupPermissionsRelations;
