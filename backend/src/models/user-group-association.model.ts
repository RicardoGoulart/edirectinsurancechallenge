import {Entity, model, property} from '@loopback/repository';

@model()
export class UserGroupAssociation extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  userid: number;

  @property({
    type: 'number',
    required: true,
  })
  groupid: number;

  @property({
    type: 'boolean',
    required: true,
    default: true,
  })
  active: boolean;

  constructor(data?: Partial<UserGroupAssociation>) {
    super(data);
  }
}

export interface UserGroupAssociationRelations {
  // describe navigational properties here
}

export type UserGroupAssociationWithRelations = UserGroupAssociation &
  UserGroupAssociationRelations;
