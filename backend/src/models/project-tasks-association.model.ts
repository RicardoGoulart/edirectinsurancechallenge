import {Entity, model, property} from '@loopback/repository';

@model()
export class ProjectTasksAssociation extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  projectid: number;

  @property({
    type: 'number',
    required: true,
  })
  taskid: number;

  constructor(data?: Partial<ProjectTasksAssociation>) {
    super(data);
  }
}

export interface ProjectTasksAssociationRelations {
  // describe navigational properties here
}

export type ProjectTasksAssociationWithRelations = ProjectTasksAssociation &
  ProjectTasksAssociationRelations;
