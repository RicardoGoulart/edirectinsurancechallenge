import {Entity, model, property} from '@loopback/repository';

@model()
export class Projects extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'string',
    required: true,
  })
  name: string;

  @property({
    type: 'string',
  })
  description?: string;

  @property({
    type: 'date',
  })
  startdate?: string;

  @property({
    type: 'date',
  })
  enddate?: string;

  constructor(data?: Partial<Projects>) {
    super(data);
  }
}

export interface ProjectsRelations {
  // describe navigational properties here
}

export type ProjectsWithRelations = Projects & ProjectsRelations;
