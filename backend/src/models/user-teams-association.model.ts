import {Entity, model, property} from '@loopback/repository';

@model()
export class UserTeamsAssociation extends Entity {
  @property({
    type: 'number',
    id: true,
    generated: true,
  })
  id?: number;

  @property({
    type: 'number',
    required: true,
  })
  userid: number;

  @property({
    type: 'number',
    required: true,
  })
  teamid: number;

  constructor(data?: Partial<UserTeamsAssociation>) {
    super(data);
  }
}

export interface UserTeamsAssociationRelations {
  // describe navigational properties here
}

export type UserTeamsAssociationWithRelations = UserTeamsAssociation &
  UserTeamsAssociationRelations;
