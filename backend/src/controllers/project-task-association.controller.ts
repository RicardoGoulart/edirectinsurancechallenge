import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {ProjectTasksAssociation} from '../models';
import {ProjectTasksAssociationRepository} from '../repositories';

export class ProjectTaskAssociationController {
  constructor(
    @repository(ProjectTasksAssociationRepository)
    public projectTasksAssociationRepository: ProjectTasksAssociationRepository,
  ) {}

  @post('/project-tasks-associations', {
    responses: {
      '200': {
        description: 'ProjectTasksAssociation model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProjectTasksAssociation),
          },
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectTasksAssociation, {
            title: 'NewProjectTasksAssociation',
            exclude: ['id'],
          }),
        },
      },
    })
    projectTasksAssociation: Omit<ProjectTasksAssociation, 'id'>,
  ): Promise<ProjectTasksAssociation> {
    return this.projectTasksAssociationRepository.create(
      projectTasksAssociation,
    );
  }

  @get('/project-tasks-associations/count', {
    responses: {
      '200': {
        description: 'ProjectTasksAssociation model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(ProjectTasksAssociation)
    where?: Where<ProjectTasksAssociation>,
  ): Promise<Count> {
    return this.projectTasksAssociationRepository.count(where);
  }

  @get('/project-tasks-associations', {
    responses: {
      '200': {
        description: 'Array of ProjectTasksAssociation model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(ProjectTasksAssociation, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(ProjectTasksAssociation)
    filter?: Filter<ProjectTasksAssociation>,
  ): Promise<ProjectTasksAssociation[]> {
    return this.projectTasksAssociationRepository.find(filter);
  }

  @patch('/project-tasks-associations', {
    responses: {
      '200': {
        description: 'ProjectTasksAssociation PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectTasksAssociation, {partial: true}),
        },
      },
    })
    projectTasksAssociation: ProjectTasksAssociation,
    @param.where(ProjectTasksAssociation)
    where?: Where<ProjectTasksAssociation>,
  ): Promise<Count> {
    return this.projectTasksAssociationRepository.updateAll(
      projectTasksAssociation,
      where,
    );
  }

  @get('/project-tasks-associations/{id}', {
    responses: {
      '200': {
        description: 'ProjectTasksAssociation model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(ProjectTasksAssociation, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(ProjectTasksAssociation, {exclude: 'where'})
    filter?: FilterExcludingWhere<ProjectTasksAssociation>,
  ): Promise<ProjectTasksAssociation> {
    return this.projectTasksAssociationRepository.findById(id, filter);
  }

  @patch('/project-tasks-associations/{id}', {
    responses: {
      '204': {
        description: 'ProjectTasksAssociation PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(ProjectTasksAssociation, {partial: true}),
        },
      },
    })
    projectTasksAssociation: ProjectTasksAssociation,
  ): Promise<void> {
    await this.projectTasksAssociationRepository.updateById(
      id,
      projectTasksAssociation,
    );
  }

  @put('/project-tasks-associations/{id}', {
    responses: {
      '204': {
        description: 'ProjectTasksAssociation PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() projectTasksAssociation: ProjectTasksAssociation,
  ): Promise<void> {
    await this.projectTasksAssociationRepository.replaceById(
      id,
      projectTasksAssociation,
    );
  }

  @del('/project-tasks-associations/{id}', {
    responses: {
      '204': {
        description: 'ProjectTasksAssociation DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.projectTasksAssociationRepository.deleteById(id);
  }
}
