import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {TeamsProjectAssociation} from '../models';
import {TeamsProjectAssociationRepository} from '../repositories';

export class TeamsProjectAssociationController {
  constructor(
    @repository(TeamsProjectAssociationRepository)
    public teamsProjectAssociationRepository: TeamsProjectAssociationRepository,
  ) {}

  @post('/teams-project-associations', {
    responses: {
      '200': {
        description: 'TeamsProjectAssociation model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(TeamsProjectAssociation),
          },
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TeamsProjectAssociation, {
            title: 'NewTeamsProjectAssociation',
            exclude: ['id'],
          }),
        },
      },
    })
    teamsProjectAssociation: Omit<TeamsProjectAssociation, 'id'>,
  ): Promise<TeamsProjectAssociation> {
    return this.teamsProjectAssociationRepository.create(
      teamsProjectAssociation,
    );
  }

  @get('/teams-project-associations/count', {
    responses: {
      '200': {
        description: 'TeamsProjectAssociation model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(TeamsProjectAssociation)
    where?: Where<TeamsProjectAssociation>,
  ): Promise<Count> {
    return this.teamsProjectAssociationRepository.count(where);
  }

  @get('/teams-project-associations', {
    responses: {
      '200': {
        description: 'Array of TeamsProjectAssociation model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(TeamsProjectAssociation, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(TeamsProjectAssociation)
    filter?: Filter<TeamsProjectAssociation>,
  ): Promise<TeamsProjectAssociation[]> {
    return this.teamsProjectAssociationRepository.find(filter);
  }

  @patch('/teams-project-associations', {
    responses: {
      '200': {
        description: 'TeamsProjectAssociation PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TeamsProjectAssociation, {partial: true}),
        },
      },
    })
    teamsProjectAssociation: TeamsProjectAssociation,
    @param.where(TeamsProjectAssociation)
    where?: Where<TeamsProjectAssociation>,
  ): Promise<Count> {
    return this.teamsProjectAssociationRepository.updateAll(
      teamsProjectAssociation,
      where,
    );
  }

  @get('/teams-project-associations/{id}', {
    responses: {
      '200': {
        description: 'TeamsProjectAssociation model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(TeamsProjectAssociation, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(TeamsProjectAssociation, {exclude: 'where'})
    filter?: FilterExcludingWhere<TeamsProjectAssociation>,
  ): Promise<TeamsProjectAssociation> {
    return this.teamsProjectAssociationRepository.findById(id, filter);
  }

  @patch('/teams-project-associations/{id}', {
    responses: {
      '204': {
        description: 'TeamsProjectAssociation PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(TeamsProjectAssociation, {partial: true}),
        },
      },
    })
    teamsProjectAssociation: TeamsProjectAssociation,
  ): Promise<void> {
    await this.teamsProjectAssociationRepository.updateById(
      id,
      teamsProjectAssociation,
    );
  }

  @put('/teams-project-associations/{id}', {
    responses: {
      '204': {
        description: 'TeamsProjectAssociation PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() teamsProjectAssociation: TeamsProjectAssociation,
  ): Promise<void> {
    await this.teamsProjectAssociationRepository.replaceById(
      id,
      teamsProjectAssociation,
    );
  }

  @del('/teams-project-associations/{id}', {
    responses: {
      '204': {
        description: 'TeamsProjectAssociation DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.teamsProjectAssociationRepository.deleteById(id);
  }
}
