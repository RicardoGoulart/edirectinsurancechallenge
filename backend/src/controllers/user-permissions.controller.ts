import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserPermissions} from '../models';
import {UserPermissionsRepository} from '../repositories';

export class UserPermissionsController {
  constructor(
    @repository(UserPermissionsRepository)
    public userPermissionsRepository: UserPermissionsRepository,
  ) {}

  @post('/user-permissions', {
    responses: {
      '200': {
        description: 'UserPermissions model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(UserPermissions)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserPermissions, {
            title: 'NewUserPermissions',
            exclude: ['id'],
          }),
        },
      },
    })
    userPermissions: Omit<UserPermissions, 'id'>,
  ): Promise<UserPermissions> {
    return this.userPermissionsRepository.create(userPermissions);
  }

  @get('/user-permissions/count', {
    responses: {
      '200': {
        description: 'UserPermissions model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(UserPermissions) where?: Where<UserPermissions>,
  ): Promise<Count> {
    return this.userPermissionsRepository.count(where);
  }

  @get('/user-permissions', {
    responses: {
      '200': {
        description: 'Array of UserPermissions model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserPermissions, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(UserPermissions) filter?: Filter<UserPermissions>,
  ): Promise<UserPermissions[]> {
    return this.userPermissionsRepository.find(filter);
  }

  @patch('/user-permissions', {
    responses: {
      '200': {
        description: 'UserPermissions PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserPermissions, {partial: true}),
        },
      },
    })
    userPermissions: UserPermissions,
    @param.where(UserPermissions) where?: Where<UserPermissions>,
  ): Promise<Count> {
    return this.userPermissionsRepository.updateAll(userPermissions, where);
  }

  @get('/user-permissions/{id}', {
    responses: {
      '200': {
        description: 'UserPermissions model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserPermissions, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(UserPermissions, {exclude: 'where'})
    filter?: FilterExcludingWhere<UserPermissions>,
  ): Promise<UserPermissions> {
    return this.userPermissionsRepository.findById(id, filter);
  }

  @patch('/user-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserPermissions PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserPermissions, {partial: true}),
        },
      },
    })
    userPermissions: UserPermissions,
  ): Promise<void> {
    await this.userPermissionsRepository.updateById(id, userPermissions);
  }

  @put('/user-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserPermissions PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userPermissions: UserPermissions,
  ): Promise<void> {
    await this.userPermissionsRepository.replaceById(id, userPermissions);
  }

  @del('/user-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserPermissions DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userPermissionsRepository.deleteById(id);
  }
}
