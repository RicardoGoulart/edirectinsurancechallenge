import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserGroupAssociation} from '../models';
import {UserGroupAssociationRepository} from '../repositories';

export class UserGroupAssociationController {
  constructor(
    @repository(UserGroupAssociationRepository)
    public userGroupAssociationRepository: UserGroupAssociationRepository,
  ) {}

  @post('/user-group-associations', {
    responses: {
      '200': {
        description: 'UserGroupAssociation model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(UserGroupAssociation)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupAssociation, {
            title: 'NewUserGroupAssociation',
            exclude: ['id'],
          }),
        },
      },
    })
    userGroupAssociation: Omit<UserGroupAssociation, 'id'>,
  ): Promise<UserGroupAssociation> {
    return this.userGroupAssociationRepository.create(userGroupAssociation);
  }

  @get('/user-group-associations/count', {
    responses: {
      '200': {
        description: 'UserGroupAssociation model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(UserGroupAssociation) where?: Where<UserGroupAssociation>,
  ): Promise<Count> {
    return this.userGroupAssociationRepository.count(where);
  }

  @get('/user-group-associations', {
    responses: {
      '200': {
        description: 'Array of UserGroupAssociation model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserGroupAssociation, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(UserGroupAssociation) filter?: Filter<UserGroupAssociation>,
  ): Promise<UserGroupAssociation[]> {
    return this.userGroupAssociationRepository.find(filter);
  }

  @patch('/user-group-associations', {
    responses: {
      '200': {
        description: 'UserGroupAssociation PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupAssociation, {partial: true}),
        },
      },
    })
    userGroupAssociation: UserGroupAssociation,
    @param.where(UserGroupAssociation) where?: Where<UserGroupAssociation>,
  ): Promise<Count> {
    return this.userGroupAssociationRepository.updateAll(
      userGroupAssociation,
      where,
    );
  }

  @get('/user-group-associations/{id}', {
    responses: {
      '200': {
        description: 'UserGroupAssociation model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserGroupAssociation, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(UserGroupAssociation, {exclude: 'where'})
    filter?: FilterExcludingWhere<UserGroupAssociation>,
  ): Promise<UserGroupAssociation> {
    return this.userGroupAssociationRepository.findById(id, filter);
  }

  @patch('/user-group-associations/{id}', {
    responses: {
      '204': {
        description: 'UserGroupAssociation PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupAssociation, {partial: true}),
        },
      },
    })
    userGroupAssociation: UserGroupAssociation,
  ): Promise<void> {
    await this.userGroupAssociationRepository.updateById(
      id,
      userGroupAssociation,
    );
  }

  @put('/user-group-associations/{id}', {
    responses: {
      '204': {
        description: 'UserGroupAssociation PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userGroupAssociation: UserGroupAssociation,
  ): Promise<void> {
    await this.userGroupAssociationRepository.replaceById(
      id,
      userGroupAssociation,
    );
  }

  @del('/user-group-associations/{id}', {
    responses: {
      '204': {
        description: 'UserGroupAssociation DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userGroupAssociationRepository.deleteById(id);
  }
}
