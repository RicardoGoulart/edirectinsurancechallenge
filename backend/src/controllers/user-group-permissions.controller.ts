import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserGroupPermissions} from '../models';
import {UserGroupPermissionsRepository} from '../repositories';

export class UserGroupPermissionsController {
  constructor(
    @repository(UserGroupPermissionsRepository)
    public userGroupPermissionsRepository: UserGroupPermissionsRepository,
  ) {}

  @post('/user-group-permissions', {
    responses: {
      '200': {
        description: 'UserGroupPermissions model instance',
        content: {
          'application/json': {schema: getModelSchemaRef(UserGroupPermissions)},
        },
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupPermissions, {
            title: 'NewUserGroupPermissions',
            exclude: ['id'],
          }),
        },
      },
    })
    userGroupPermissions: Omit<UserGroupPermissions, 'id'>,
  ): Promise<UserGroupPermissions> {
    return this.userGroupPermissionsRepository.create(userGroupPermissions);
  }

  @get('/user-group-permissions/count', {
    responses: {
      '200': {
        description: 'UserGroupPermissions model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(UserGroupPermissions) where?: Where<UserGroupPermissions>,
  ): Promise<Count> {
    return this.userGroupPermissionsRepository.count(where);
  }

  @get('/user-group-permissions', {
    responses: {
      '200': {
        description: 'Array of UserGroupPermissions model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserGroupPermissions, {
                includeRelations: true,
              }),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(UserGroupPermissions) filter?: Filter<UserGroupPermissions>,
  ): Promise<UserGroupPermissions[]> {
    return this.userGroupPermissionsRepository.find(filter);
  }

  @patch('/user-group-permissions', {
    responses: {
      '200': {
        description: 'UserGroupPermissions PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupPermissions, {partial: true}),
        },
      },
    })
    userGroupPermissions: UserGroupPermissions,
    @param.where(UserGroupPermissions) where?: Where<UserGroupPermissions>,
  ): Promise<Count> {
    return this.userGroupPermissionsRepository.updateAll(
      userGroupPermissions,
      where,
    );
  }

  @get('/user-group-permissions/{id}', {
    responses: {
      '200': {
        description: 'UserGroupPermissions model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserGroupPermissions, {
              includeRelations: true,
            }),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(UserGroupPermissions, {exclude: 'where'})
    filter?: FilterExcludingWhere<UserGroupPermissions>,
  ): Promise<UserGroupPermissions> {
    return this.userGroupPermissionsRepository.findById(id, filter);
  }

  @patch('/user-group-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserGroupPermissions PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroupPermissions, {partial: true}),
        },
      },
    })
    userGroupPermissions: UserGroupPermissions,
  ): Promise<void> {
    await this.userGroupPermissionsRepository.updateById(
      id,
      userGroupPermissions,
    );
  }

  @put('/user-group-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserGroupPermissions PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userGroupPermissions: UserGroupPermissions,
  ): Promise<void> {
    await this.userGroupPermissionsRepository.replaceById(
      id,
      userGroupPermissions,
    );
  }

  @del('/user-group-permissions/{id}', {
    responses: {
      '204': {
        description: 'UserGroupPermissions DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userGroupPermissionsRepository.deleteById(id);
  }
}
