import {
  Count,
  CountSchema,
  Filter,
  FilterExcludingWhere,
  repository,
  Where,
} from '@loopback/repository';
import {
  post,
  param,
  get,
  getModelSchemaRef,
  patch,
  put,
  del,
  requestBody,
} from '@loopback/rest';
import {UserGroups} from '../models';
import {UserGroupsRepository} from '../repositories';

export class UserGroupsController {
  constructor(
    @repository(UserGroupsRepository)
    public userGroupsRepository: UserGroupsRepository,
  ) {}

  @post('/user-groups', {
    responses: {
      '200': {
        description: 'UserGroups model instance',
        content: {'application/json': {schema: getModelSchemaRef(UserGroups)}},
      },
    },
  })
  async create(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroups, {
            title: 'NewUserGroups',
            exclude: ['id'],
          }),
        },
      },
    })
    userGroups: Omit<UserGroups, 'id'>,
  ): Promise<UserGroups> {
    return this.userGroupsRepository.create(userGroups);
  }

  @get('/user-groups/count', {
    responses: {
      '200': {
        description: 'UserGroups model count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async count(
    @param.where(UserGroups) where?: Where<UserGroups>,
  ): Promise<Count> {
    return this.userGroupsRepository.count(where);
  }

  @get('/user-groups', {
    responses: {
      '200': {
        description: 'Array of UserGroups model instances',
        content: {
          'application/json': {
            schema: {
              type: 'array',
              items: getModelSchemaRef(UserGroups, {includeRelations: true}),
            },
          },
        },
      },
    },
  })
  async find(
    @param.filter(UserGroups) filter?: Filter<UserGroups>,
  ): Promise<UserGroups[]> {
    return this.userGroupsRepository.find(filter);
  }

  @patch('/user-groups', {
    responses: {
      '200': {
        description: 'UserGroups PATCH success count',
        content: {'application/json': {schema: CountSchema}},
      },
    },
  })
  async updateAll(
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroups, {partial: true}),
        },
      },
    })
    userGroups: UserGroups,
    @param.where(UserGroups) where?: Where<UserGroups>,
  ): Promise<Count> {
    return this.userGroupsRepository.updateAll(userGroups, where);
  }

  @get('/user-groups/{id}', {
    responses: {
      '200': {
        description: 'UserGroups model instance',
        content: {
          'application/json': {
            schema: getModelSchemaRef(UserGroups, {includeRelations: true}),
          },
        },
      },
    },
  })
  async findById(
    @param.path.number('id') id: number,
    @param.filter(UserGroups, {exclude: 'where'})
    filter?: FilterExcludingWhere<UserGroups>,
  ): Promise<UserGroups> {
    return this.userGroupsRepository.findById(id, filter);
  }

  @patch('/user-groups/{id}', {
    responses: {
      '204': {
        description: 'UserGroups PATCH success',
      },
    },
  })
  async updateById(
    @param.path.number('id') id: number,
    @requestBody({
      content: {
        'application/json': {
          schema: getModelSchemaRef(UserGroups, {partial: true}),
        },
      },
    })
    userGroups: UserGroups,
  ): Promise<void> {
    await this.userGroupsRepository.updateById(id, userGroups);
  }

  @put('/user-groups/{id}', {
    responses: {
      '204': {
        description: 'UserGroups PUT success',
      },
    },
  })
  async replaceById(
    @param.path.number('id') id: number,
    @requestBody() userGroups: UserGroups,
  ): Promise<void> {
    await this.userGroupsRepository.replaceById(id, userGroups);
  }

  @del('/user-groups/{id}', {
    responses: {
      '204': {
        description: 'UserGroups DELETE success',
      },
    },
  })
  async deleteById(@param.path.number('id') id: number): Promise<void> {
    await this.userGroupsRepository.deleteById(id);
  }
}
